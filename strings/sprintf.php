<?php

printf('%s', 'This is a string');
echo PHP_EOL;
printf('%d', 1234567890.08);
echo PHP_EOL;
printf('%f', 12.25);
echo PHP_EOL;
printf('%o', 0777);
echo PHP_EOL;
printf('%e', 3);
echo PHP_EOL;
printf('%012d', 1234);
echo PHP_EOL;
printf('%.2f', 14.249);
echo PHP_EOL;
