<?php

echo 16 >> 2;
echo PHP_EOL;
echo ~13;
echo PHP_EOL;
echo 4 ^ ~29;
echo PHP_EOL;
echo 2 & 29;
echo PHP_EOL;

/*
 * -/+ 16 8 4 2 1
 *   0  0 0 1 0 0
 *   0  1 1 1 0 1
 *   1  0 0 0 1 0
 *   0  1 0 0 0 0
 *   1  0 1 1 1 1
 *      1 1 1 1 1
 */