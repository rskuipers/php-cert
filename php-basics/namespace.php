<?php

namespace myapp {

    hello\world();
}

namespace myapp\hello {
    function world() {
        echo 'Hello world!' . PHP_EOL;
    }
}