<?php

$str = 'Sometimes we drink beer';

echo $str[0];
echo PHP_EOL;

$str = 'Ümlaut';

echo $str[0];
echo PHP_EOL;
echo $str[1];
echo PHP_EOL;
echo $str[2];
echo PHP_EOL;
echo mb_substr($str, 0, 1, 'UTF-8');
echo PHP_EOL;
echo substr(utf8_encode($str), 0, 1);
echo PHP_EOL;
echo PHP_EOL;
echo strlen($str);
echo PHP_EOL;
echo mb_strlen($str);
echo PHP_EOL;
echo PHP_EOL;
