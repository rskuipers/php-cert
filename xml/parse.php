<?php

$parser = xml_parser_create();
xml_set_element_handler($parser, function($k) {
    echo 'Start! ' . $k;
}, function() {
    echo 'End!';
});
xml_parse($parser, '<test><hi>1</hi></test>');