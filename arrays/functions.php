<?php

$arr = ['A' => 'Apple', 'B' => 'Banana', 'C' => 'Cherry'];

echo 'array_change_key_case' . PHP_EOL;
var_dump(array_change_key_case($arr));
echo PHP_EOL;

echo 'array_chunk' . PHP_EOL;
var_dump(array_chunk($arr, 2));
echo PHP_EOL;

echo 'array_column' . PHP_EOL;
$arr2 = [['name' => 'John', 'last_name' => 'Doe'], ['name' => 'Bob', 'last_name' => 'Bing']];
var_dump(array_column($arr2, 'name'));
echo PHP_EOL;

echo 'array_combine' . PHP_EOL;
$arr2 = ['First', 'Second', 'Third'];
var_dump(array_combine($arr2, $arr));
echo PHP_EOL;

echo 'array_count_values' . PHP_EOL;
var_dump(array_count_values($arr));
echo PHP_EOL;

echo 'array_diff_assoc' . PHP_EOL;
var_dump(array_diff_assoc($arr, ['A' => 'Apple', 'Banana', 'c' => 'Cherry']));
echo PHP_EOL;

echo 'array_diff_key' . PHP_EOL;
var_dump(array_diff_key($arr, ['A' => 'Arm', 'b' => 'Banana']));
echo PHP_EOL;

echo 'array_diff_uassoc' . PHP_EOL;
var_dump(array_diff_uassoc($arr, ['A' => 'Apple', 'Banana', 'c' => 'Cherry'], function ($a, $b) {
    if (strcasecmp($a, $b) === 0) {
        return 0;
    }
    return ($a > $b)? 1:-1;
}));
echo PHP_EOL;

echo 'array_fill_keys' . PHP_EOL;
var_dump(array_fill_keys($arr, 'HA'));
echo PHP_EOL;

echo 'array_fill' . PHP_EOL;
var_dump(array_fill(0, 12, 'Hi!'));
echo PHP_EOL;

echo 'array_filter' . PHP_EOL;
var_dump(array_filter($arr, function ($var) {
    return $var == 'Banana';
}));
echo PHP_EOL;

echo 'array_flip' . PHP_EOL;
var_dump(array_flip($arr));
echo PHP_EOL;

echo 'array_map' . PHP_EOL;
var_dump(array_map('trim', $arr));

echo 'array_reduce' . PHP_EOL;
var_dump(array_reduce($arr, function($carry, $item) {
    if ($item != 'Banana') {
        $carry .= ' ' . $item;
    }
    return $carry;
}, 'fruits'));
echo PHP_EOL;

echo 'array_search' . PHP_EOL;
$array = array(0 => 'blue', 1 => 'red', 2 => 'green', 3 => 'red');
var_dump(array_search('green', $array), in_array('green', $array));
