<?php

$arr1 = ['One' => 'Apple', 'Two' => 'Banana'];
$arr2 = ['One' => 'Cherry', 'Two' => 'Date'];

var_dump(array_merge($arr1, $arr2));

$arr1 = ['0' => 'Apple', '1' => 'Banana'];
$arr2 = ['0' => 'Cherry', '1' => 'Date'];

var_dump(array_merge($arr1, $arr2));
