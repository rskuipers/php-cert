<?php
echo 'Push:' . PHP_EOL;
$arr = [1, 2, 3];
array_push($arr, 4);
var_dump($arr);
array_pop($arr);
var_dump($arr);
echo PHP_EOL;
echo 'Shift:' . PHP_EOL;
array_unshift($arr, 0);
var_dump($arr);
array_shift($arr);
var_dump($arr);

